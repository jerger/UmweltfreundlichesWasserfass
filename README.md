Dieses Projekt ist umgezogen nach: https://repo.prod.meissa.de/jem/UmweltfreundlichesWasserfass

# Wein-Regenfass

Da bei uns hier das Wetter ja eher extremer wird (mehr trocken und mehr nass), dachten wir uns dass wir unsere Regenwassersammlung jetzt ja auch ein bischen ausbauen können.

Um unser Gewissen nicht mit noch mehr Plastik belasten zu müssen haben wir eine umweltfreundlichen Weg ausgeknobelt - mit alten Wein / Sherry / Whiskey Fässern.

## So sieht das ganze aus

![](ueberblick.jpg)

![](verbindungDachrinne.jpg)

![](verbindungTopTop1.jpg)

![](verbindungTopTop2.jpg)

![](verbindungTopDown.jpg)

## Funktionsweise

![](schema.png)

Ich habe beim Aufbau auf Folgendes geachtet:
1. Der Regensammler an der Dachrinne bestimmt den Wasserstand in allen Fässern. D.h. das höchste Fass inkl. Unterlage sollts seine Oberkante knapp oberhalb des Regensammler- Auslasses haben. Stehen die Fässer zu tief, laufen sie über. Stehen sie zu hoch, dann werden sie nicht ganz voll. Der erste Regen beweist die richtige Höhe :-)
2. Zur Verbindung von zwei Fässern habe ich die Variante oben-oben ausprobiert. Der Versatz kommt von den Fassringen, die ich natürlich nicht durchbohren möchte. Der Vorteil hier ist, das zweite Fass wird erst gefüllt, wenn das erste voll ist.
3. Die zweite Verbindungsvariante ist oben-unten. AUch hier wird das dritte Fass erst dann gefüllt, wenn das zweite voll ist. Aber ich kann das dritte Fass so mit dem großen Schlauch auslaufen lassen. Allerdings ist mein verwendeter Schlauchetwas knickfreudig und sobald irgend ein Teil undicht ist, dan tropft es natürlich.
4. Als Unterlage verwende ich Ziegelsteine. Die halten dem Druck (600kg) stand, verotten nicht und ich hatte noch welche. Auch mit hohem Unterbau hatte ich noch keine Kipprobleme ...
5. Last but not least, der Auslaufhan ist idealerweise etwas oberhalb meiner Gieskanne :-)

## Montage Schritte

Interessant ist eigentlich nur die Bohrung. So ein bis 2mm zu klein ist bei Eiche eine gute Größe. Damit haben die Gewinde noch genügend Gripp und die Teile lassen sich ohne Gewindeschneiden eindrehen.
1. Für 1 Zoll Anschlüsse ist das der 32mm Bohrer 

   ![](Werkzeug/foerstner32.jpg),

2. Für 3/4 Zoll Anschlüsse ist das der 20mm Bohrer 

   ![](Werkzeug/foerstner20.jpg).


## Materialliste

1. Fitting Doppelnipel 1'', Messing https://www.stabilo-sanitaer.de/de/fitting-doppelnippel-1-zoll-dn25-messing/a-437096/

   ![](Material/fitting_dn.jpg)
2. Fitting Kappe 1'', Messing https://www.stabilo-sanitaer.de/de/kappe-1-zoll-messing-ig-endkappe-verschlusskappe/a-437543/

   ![](Material/fitting_kappe.jpg) 
3. Schlauch-Anschluss https://www.stabilo-sanitaer.de/schlauchverschraubung-1-zoll-x-1-zoll-25mm-reduziert-2-teilig/a-440350/
   
4. Wassarhahn 3/4'' z.B. https://www.stabilo-sanitaer.de/kugelauslaufhahn-1-2-messing-wasserhahn-mit-schlauchtuelle/a-443269/

   ![](Material/hahn.jpg)
5. Gebrauchte Weinfäser überarbeitet: https://www.faesser-shop.de/fassgarten/regentonnen/regentonne-500-l-great-britain?c=15
   
6. Regensammler https://blechshop24.com/fallrohre/regenwasserklappen-verteiler-sammler/zink/73/zink-wassersammler

   ![](Material/regensammler.jpg)
7. Hanf zum Gewinde dichten 

   ![](Material/hanf.jpg)
8. Dichtung für Fitting-Kappen: Gummi 44x32x3 online-dichtungsshop.de

   ![](Material/dichtungKappe.jpg) 
9. Wasserschlauch 1''

   ![](Material/Wasserschlauch.jpg)
10. Zeigelstein zum Unterstellen 

   ![](Material/ziegelstein.jpg)
    
