# Materialliste

1. Fitting Doppelnipel 1'', Messing https://www.stabilo-sanitaer.de/de/fitting-doppelnippel-1-zoll-dn25-messing/a-437096/

   ![](fitting_dn.jpg)
2. Fitting Kappe 1'', Messing https://www.stabilo-sanitaer.de/de/kappe-1-zoll-messing-ig-endkappe-verschlusskappe/a-437543/

   ![](fitting_kappe.jpg) 
3. Schlauch-Anschluss https://www.stabilo-sanitaer.de/schlauchverschraubung-1-zoll-x-1-zoll-25mm-reduziert-2-teilig/a-440350/
   
4. Wassarhahn 3/4'' z.B. https://www.stabilo-sanitaer.de/kugelauslaufhahn-1-2-messing-wasserhahn-mit-schlauchtuelle/a-443269/

   ![](hahn.jpg)
5. Gebrauchte Weinfäser überarbeitet: https://www.faesser-shop.de/fassgarten/regentonnen/regentonne-500-l-great-britain?c=15
   
6. Regensammler https://blechshop24.com/fallrohre/regenwasserklappen-verteiler-sammler/zink/73/zink-wassersammler

   ![](regensammler.jpg)
7. Hanf zum Gewinde dichten 

   ![](hanf.jpg)
8. Dichtung für Fitting-Kappen: Gummi 44x32x3 online-dichtungsshop.de

   ![](dichtungKappe.jpg) 
9. Wasserschlauch 1''

   ![](Wasserschlauch.jpg)
10. Zeigelstein zum Unterstellen 

   ![](ziegelstein.jpg)
